# Blender

Blender is a library which is there to help with your daily celery chopping chores. You have a big celery to cook and you want to chop it in millions of pieces?
What if your losing some? What if some get eaten by the celery monster? Or anyone else? What if one piece was rotten?

With this blender you will have control over all that.

## Let's be real

You have a task
```python
@task
def resizeThumbnail(url):
    """ does whatever a spiderpig does """
    pass
```

But you have a million of those to resize... So what you would like is:
```python
@taskgenerator(task='mytasks.resizeThumbnail')
def resizeGenerator():
    for product in myproductstreamer():
        yield product.image_url
```

or a database thing:
```python
@taskgenerator(task='mytasks.resizeThumbnail', chunks=100)
def resizeGenerator(offset, limit):
    sql = myproductfetcher()
    for product in sql.offset(index).limit(limit):
        yield product.image_url
```

And soon plenty of cool features like this. The point is it will generate all the tasks for you and register in a separate database a bunch of info about those tasks. So when you launch:
```bash
celery blender
```

you will have a flask server running showing you statistics like:
 * When was this task launched
 * how many tasks are currently processing, failed, delayed, not yet taken...
 * when will this thing finish

you also have control and can:
 * stop it all
 * see exceptions and results of tasks grouped by similarity
 * you can fix the temporary problems and relaunch all the failed tasks
 * you can see what bloody tasks have been dropped by rabbitMQ and make sure those are being done afterall

All that in a cool interface
[your imagination will definitely see screenshots here] 

##Stay tuned!

Everything will come soon...